﻿using System;
using System.Collections.Generic;

namespace BD_13
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();
            for (int i = 0; i < 10; i++)
            {
                users.Add(new User($"name{i.ToString()}",
                    $"patronymic{i.ToString()}",
                    $"surname{i.ToString()}",
                    $"login{i.ToString()}",
                    $"password{i.ToString()}"));
            }

            using (var db = new DB("..."))
            {
                db.AddUsers(users);
                
                db.ShowAllRecordsByTableName("User");
                db.ShowAllRecordsByTableName("Account");
                db.ShowAllRecordsByTableName("HistoryOperation");
            }

        }
    }
}