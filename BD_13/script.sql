USE master;
GO
IF DB_ID ('Bank') IS NOT NULL
DROP DATABASE Bank;
GO
CREATE DATABASE Bank
 ON  PRIMARY 
( NAME = 'Bank', FILENAME = N'E:\Bank.mdf'  )

ALTER DATABASE Bank SET QUERY_STORE = OFF
GO
USE Bank
GO
CREATE TABLE [User](
	Id int NOT NULL Identity(1,1),
	CONSTRAINT PK_User PRIMARY KEY CLUSTERED (Id),
	Login nchar(50) NULL,
	Password nchar(50) NULL,
	Name nchar(50) NULL,
	Patronymic nchar(50) NULL,
	Surname nchar(10) NULL
) ON [PRIMARY]
GO
CREATE TABLE Account(
	Id int NOT NULL Identity(1,1),
	 CONSTRAINT PK_Account PRIMARY KEY CLUSTERED (Id),
	 CONSTRAINT FK_User_Account FOREIGN KEY(Id_user)
		REFERENCES [dbo].[User] (Id),
	CreateDate date NOT NULL,
	Sum money NOT NULL,
	Id_user int NOT NULL,
) ON [PRIMARY]

GO
CREATE TABLE HistoryOperation(
	Id int NOT NULL Identity(1,1),
	CONSTRAINT PK_HistoryOperation PRIMARY KEY CLUSTERED (Id),
	CONSTRAINT FK_HistoryOperation_Account FOREIGN KEY(Id_account)
		REFERENCES Account (Id),
	OperationDate datetime NOT NULL,
	Sum money NOT NULL,
	Id_account int NOT NULL
) ON [PRIMARY]
GO

INSERT INTO [User] (Login, Password, Name, Patronymic, Surname) 
	VALUES ( 'user1', 'password1', 'User1', 'Patronymic1', 'Surname1');  
INSERT INTO [User] (Login, Password, Name, Patronymic, Surname) 
	VALUES ( 'user2', 'password2', 'User2', 'Patronymic2', 'Surname2');  
INSERT INTO [User] (Login, Password, Name, Patronymic, Surname) 
	VALUES ( 'user3', 'password3', 'User3', 'Patronymic3', 'Surname3');  
INSERT INTO [User] (Login, Password, Name, Patronymic, Surname) 
	VALUES ( 'user4', 'password2', 'User4', 'Patronymic4', 'Surname4');  

INSERT INTO Account(CreateDate, Sum, Id_user) 
	VALUES ('2010-01-15', 2000, 1)
INSERT INTO Account(CreateDate, Sum, Id_user) 
	VALUES ('2010-01-27', 2100, 2)
INSERT INTO Account(CreateDate, Sum, Id_user) 
	VALUES ('2010-02-11', 980, 3)
INSERT INTO Account(CreateDate, Sum, Id_user) 
	VALUES ('2010-05-28', 700, 4)
INSERT INTO Account(CreateDate, Sum, Id_user) 
	VALUES ('2010-06-10', 1400, 4)


INSERT INTO HistoryOperation(OperationDate, Sum, Id_account) 
	VALUES ('2010-15-01 12:35:18', 2000, 1)
INSERT INTO HistoryOperation(OperationDate, Sum, Id_account) 
	VALUES ('2010-27-01 12:27:02', 2100, 2)
INSERT INTO HistoryOperation(OperationDate, Sum, Id_account) 
	VALUES ('2010-11-02 12:45:32', 980, 3)
INSERT INTO HistoryOperation(OperationDate, Sum, Id_account) 
	VALUES ('2010-28-05 12:17:32', 700, 4)
INSERT INTO HistoryOperation(OperationDate, Sum, Id_account) 
	VALUES ('2010-10-06 12:35:18', 1400, 5)
GO
USE master
GO
ALTER DATABASE Bank SET  READ_WRITE 
GO
