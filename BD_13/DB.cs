﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BD_13
{
    public class DB : IDisposable
    {
        private readonly SqlConnection _connection;

        public DB(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public void AddUser(User user)
        {
            OpenConnection();
            SqlCommand command = new SqlCommand($"INSERT INTO User (Login, Password, Name, Patronymic, Surname) " +
                                                $"VALUES ( '{user.Login}', " +
                                                $"'{user.Password}', " +
                                                $"'{user.Name}'," +
                                                $"'{user.Patronymic}'," +
                                                $" '{user.Surname}')", _connection);
            command.ExecuteNonQuery();
            CloseConnection();
        }

        public void ShowAllRecordsByTableName(string tableName)
        {
            OpenConnection();
            StringBuilder result = new StringBuilder();
            SqlCommand command = new SqlCommand($"select * from [{tableName}]", _connection);
            
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    String row = string.Empty;
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        row += $"{reader.GetName(i)}: {reader[i].ToString()}";
                    }
                    result.Append(row);
                }
            }

            CloseConnection();
            
            Console.WriteLine(result.ToString());
        }

        public void AddUsers(IEnumerable<User> users)
        {
            OpenConnection();
            var transaction = _connection.BeginTransaction();
            foreach (var user in users)
            {
                SqlCommand command = new SqlCommand($"INSERT INTO [User] (Login, Password, Name, Patronymic, Surname) " +
                                                    $"VALUES ( '{user.Login}', " +
                                                    $"'{user.Password}', " +
                                                    $"'{user.Name}'," +
                                                    $"'{user.Patronymic}'," +
                                                    $" '{user.Surname}')", _connection, transaction);
                command.ExecuteNonQuery();
            }
            transaction.Commit();
            CloseConnection();
        }

        private void OpenConnection()
        {
            if(_connection.State == ConnectionState.Closed)
                _connection.Open();
        }
        private void CloseConnection()
        {
            if(_connection.State != ConnectionState.Closed)
                _connection.Close();
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}