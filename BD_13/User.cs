﻿using System;

namespace BD_13
{
    public class User
    {
        public User(string name, string patronymic, string surname, string login, string password)
        {
            Name = name;
            Patronymic = patronymic;
            Surname = surname;
            Login = login;
            Password = password;
        }

        public string Name { get; }
        public string Patronymic { get; }
        public string Surname { get; }
        public string Login { get; }
        public string Password { get; }
    }
}